<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_send_mail_with_smtp
 * @version   1.0.0
 * @since     2013.10.17.
 */
require_once('Mail/Address.php');
require_once('Mail/Header.php');
require_once('Mail/Part.php');
require_once('Mail/Adapter.php');
require_once('Mail/Adapter/Abstract.php');
/**
 * Class Mail
 */
class Mail {
    /**
     * Mail end of line sign.
     *
     * @var string
     */
    const EOL = "\r\n";

    const HEADER_FROM = 'From';

    const HEADER_TO = 'To';

    const HEADER_CC = 'Cc';

    const HEADER_BCC = 'Bcc';

    const HEADER_SUBJECT = 'Subject';

    const HEADER_CONTENT_TYPE = 'Content-Type';

    const HEADER_MESSAGE_ID = 'Message-Id';

    const HEADER_DATE = 'Date';

    const HEADER_REPLY_TO = 'Reply-To';

    const HEADER_RETURN_PATH = 'Return-Path';

    const HEADER_X_MAILER = 'X-Mailer';

    /**
     * Use the CHARSET_LATIN_1 constant in the config of a Mail charset.
     *
     * @var string
     */
    const CHARSET_LATIN_1 = 'ISO-8859-1';

    /**
     * Use the CHARSET_LATIN_2 constant in the config of a Mail charset.
     *
     * @var string
     */
    const CHARSET_LATIN_2 = 'ISO-8859-2';

    /**
     * Use the CHARSET_UTF_8 constant in the config of a Mail charset.
     *
     * @var string
     */
    const CHARSET_UTF_8 = 'UTF-8';

    /**
     * The primary subtype of text. This indicates unformatted text.
     *
     * @var string
     */
    const CONTENT_TYPE_TEXT = 'text/plain';

    /**
     * One of the subtypes of text. This indicates html formatted text.
     *
     * @var string
     */
    const CONTENT_TYPE_HTML = 'text/html';

    const CONTENT_TYPE_MULTIPAR_MIXED = 'multipart/mixed';

    const DEFAULT_ADAPTER = 'mail';

    protected static $standardHeaders = array(
        'from',
        'to',
        'cc',
        'bcc',
        'subject',
        'content-type',
        'message-id',
        'date',
        'reply-to',
        'return-path',
        'x-mailer'
    );

    /**
     * Mail character set.
     *
     * Default: CHARSET_UTF_8
     *
     * @var string
     */
    protected $charset = self::CHARSET_UTF_8;

    /**
     * Mail content type.
     *
     * Default: CONTEN_TYPE_TEXT
     *
     * @var string
     */
    protected $contentType = self::CONTENT_TYPE_TEXT;

    /**
     * This field contains the mail headers.
     *
     * @var Mail_Header
     */
    protected $header = null;

    /**
     * This field contains the mail parts.
     *
     * @var array(Mail_Part)
     */
    protected $parts = array();

    /**
     * MIME boundary string.
     *
     * @var null|string
     */
    protected $boundary = null;

    protected $adapter = null;

    /**
     * Constructor
     *
     * If the $options is a string then it must be a valid character set.
     *
     * @param string|array $options
     *
     * @throws Mail_Exception
     */
    public function __construct($options) {
        if (is_string($options)) {
            $options = array('charset' => $options);
        } elseif (!is_array($options)) {
            self::throwException('The options must be a string or an array.');
        }

        $this->setOptions((array)$options);
    }

    /**
     * Sets some mail options.
     *
     * Supported options:
     *      charset
     *
     * @param array $options
     *
     * @return $this
     * @throws Mail_Exception
     */
    public function setOptions(array $options) {
        foreach ($options as $name => $value) {
            $method = 'set' . ucfirst($name);
            if (method_exists($this, $method)) {
                call_user_func_array(array(
                                         $this,
                                         $method
                                     ),
                                     (array)$value);
            } else {
                self::throwException('The "' . $name . '" is not supported option.');
            }

        }

        return $this;
    }

    /**
     * Sets the sender of the mail
     *
     * @param string|Mail_Address $email
     * @param null|string         $name
     *
     * @return $this
     * @throws Exception
     */
    public function setFrom($email, $name = null) {
        $email = self::getMailAddress($email, $name, $this->getCharset());
        $this->getHeader()->add(self::HEADER_FROM, $email);

        return $this;
    }

    /**
     * Returns the sender of the mail.
     *
     * @return Mail_Address
     */
    public function getFrom() {
        $from = $this->getHeader()->get(self::HEADER_FROM);

        return $from[0];
    }

    /**
     * Encode a string.
     *
     * @param string $value
     * @param string $charset
     *
     * @return string
     */
    public static function encode($value, $charset = self::CHARSET_UTF_8) {
        // TODO fixing encoding
        // https://www.ietf.org/rfc/rfc1522.txt
//        $begin = '=?' . $charset . '?B?';
//        $end = '?=';

//        return $begin . base64_encode($value) . $end;

        return $value;
    }

    /**
     * Returns the mail character set.
     *
     * @return string
     */
    public function getCharset() {
        return $this->charset;
    }

    /**
     * Sets the mail character set.
     *
     * @param string $charset
     *
     * @return $this
     * @throws Mail_Exception
     */
    public function setCharset($charset) {
        if (!is_string($charset)) {
            self::throwException('The character set must be a string.');
        }
        $this->charset = $charset;

        return $this;
    }

    /**
     * Sets the subject of the mail.
     *
     * @param string $subject
     *
     * @return $this
     * @throws Mail_Exception
     */
    public function setSubject($subject) {
        if (!is_string($subject)) {
            self::throwException('Email subject must be a string.');
        }

        $this->getHeader()->add(self::HEADER_SUBJECT, $this->encode(rtrim($subject)));

        return $this;
    }

    /**
     * Returns the subject of the mail.
     *
     * @return string
     */
    public function getSubject() {
        $subject = $this->getHeader()->get(self::HEADER_SUBJECT);

        return $subject[0];
    }

    /**
     * Adds a primary recipient of the message.
     *
     * @param string|Mail_Address $email
     * @param null|string         $name
     *
     * @return $this
     */
    public function addTo($email, $name = null) {
        $email = self::getMailAddress($email, $name, $this->getCharset());
        $this->getHeader()->add(self::HEADER_TO, $email, true);

        return $this;
    }

    /**
     * Returns the primary recipient of the message.
     *
     * @return array
     */
    public function getTo() {
        return $this->getHeader()->get(self::HEADER_TO);
    }

    /**
     * Adds a secondary recipient of the message.
     *
     * @param string|Mail_Address $email
     * @param null|string         $name
     *
     * @return $this
     */
    public function addCc($email, $name = null) {
        $email = self::getMailAddress($email, $name, $this->getCharset());
        $this->getHeader()->add(self::HEADER_CC, $email, true);

        return $this;
    }

    /**
     * Returns secondary recipient of the message.
     *
     * @return array
     */
    public function getCc() {
        return $this->getHeader()->get(self::HEADER_CC);
    }

    /**
     * Adds an additional recipient of the message.
     *
     * @param string|Mail_Address $email
     * @param null|string         $name
     *
     * @return $this
     */
    public function addBcc($email, $name = null) {
        $email = self::getMailAddress($email, $name, $this->getCharset());
        $this->getHeader()->add(self::HEADER_BCC, $email, true);

        return $this;
    }

    /**
     * Returns additional recipients of the message.
     *
     * @return array
     */
    public function getBcc() {
        return $this->getHeader()->get(self::HEADER_BCC);
    }

    /**
     * Add a mail header.
     *
     * @param string $name
     * @param string $value
     * @param bool   $append
     */
    public function header($name, $value, $append = false) {
        if (in_array(strtolower($name), self::$standardHeaders)) {
            self::throwException('Cannot set standard header.');
        }

        $this->getHeader()->add($name, $value, $append);
    }

    /**
     * Add a mail text part.
     *
     * @param string $content
     * @param string $charset
     *
     * @return $this
     */
    public function addTextBody($content, $charset = null){
        return $this->addPart($content, self::CONTENT_TYPE_TEXT, $charset);
    }
    /**
     * Add a mail html part.
     *
     * @param string $content
     * @param string $charset
     *
     * @return $this
     */
    public function addHtmlBody($content, $charset = null){
        return $this->addPart($content, self::CONTENT_TYPE_HTML, $charset);
    }

    /**
     * Sets the mail sender adapter.
     *
     * @param string|array|Mail_Adapter_Abstract $name
     * @param null|array                         $options
     *
     * @return $this
     */
    public function setAdapter($name, $options = null) {
        if ($name instanceof Mail_Adapter_Abstract) {
            $this->adapter = $name;
        } else {
            $this->adapter = Mail_Adapter::factory($name, $options);
        }

        return $this;
    }

    /**
     * Returns the mail sender adapter.
     *
     * If it is not set up then returns the default adapter.
     *
     * @return Mail_Adapter_Abstract
     */
    public function getAdapter() {
        if (null === $this->adapter) {
            $this->setAdapter(self::DEFAULT_ADAPTER);
        }

        return $this->adapter;
    }

    /**
     * Returns log messages as array.
     *
     * @return array
     */
    public function getLog() {
        return $this->getAdapter()->getLog();
    }

    /**
     * Returns log messages as string.
     *
     * @return string
     */
    public function getLogAsString() {
        return implode(PHP_EOL, $this->getLog());
    }

    /**
     * Send the mail.
     *
     * @return mixed
     */
    public function send() {
        return $this->getAdapter()->send($this);
    }


    /**
     * Returns this mail as string.
     *
     * @return string
     */
    public function __toString() {
        return $this->headerToString() . self::EOL . self::EOL . $this->bodyToString();
    }

    /**
     * Throw an exception.
     *
     * @param string    $message
     * @param int       $code
     * @param Exception $previous
     *
     * @see Mail_Exception
     * @throws Mail_Exception
     */
    public static function throwException($message, $code = 0, Exception $previous = null) {
        self::loadClass('Mail_Exception');
        throw new Mail_Exception($message, $code, $previous);
    }

    /**
     * Include a class.
     *
     * @param string $className
     *
     * @return bool
     */
    public static function loadClass($className) {
        $fileName = str_replace('_', '/', $className) . '.php';

        include_once($fileName);

        if (!class_exists($className)) {
            Mail::throwException('The "' . $className . '" in not exists.');
        }

        return true;
    }

    /**
     * Returns server name.
     *
     * @return string
     */
    public static function getHostName() {
        if (isset($_SERVER['SERVER_NAME'])) {
            $hostName = $_SERVER['SERVER_NAME'];
        } else {
            $hostName = php_uname('n');
        }

        return $hostName;
    }

    /**
     * Add a mail part.
     *
     * @param string $content
     * @param string       $type
     * @param string|null  $charset
     *
     * @return $this
     */
    protected function addPart($content, $type, $charset = null) {
        $charset = (null === $charset) ? $this->getCharset() : $charset;

        $part = new Mail_Part($content);
        $part->setType($type);
        $part->setCharset($charset);

        array_push($this->parts, $part);

        return $this;
    }

    /**
     * Return the boundary string.
     *
     * If it is not generated than make it.
     *
     * @return string
     */
    protected function getBoundary() {
        if (null === $this->boundary) {
            $this->boundary = md5($this->getMessageID());
        }

        return $this->boundary;
    }

    /**
     * Returns this mail body as string.
     *
     * @return string
     */
    public function bodyToString() {
        $body = array();

        while ($part = array_shift($this->parts)) {
            array_push($body, '', $this->boundary(), (string)$part);
        }

        array_push($body, $this->boundary(true));

        return implode(self::EOL, $body);
    }

    protected function boundary($end = false) {
        return '--' . $this->getBoundary() . (true === $end ? '--' : '');
    }

    /**
     * Returns a mail message id.
     *
     * @return string
     */
    protected function getMessageID() {
        $id = (string)$this->getFrom() . microtime(true) . mt_rand();

        return sha1($id) . '@' . self::getHostName();
    }

    /**
     * Returns this mail headers as string.
     *
     * @return string
     */
    public function headerToString() {
        // TODO refactor -> perhaps setStandardHeaders
        $this->getHeader()->add(self::HEADER_DATE, date('r'));
        $this->getHeader()->add(self::HEADER_MESSAGE_ID, '<' . $this->getMessageID() . '>');
        $this->getHeader()->add(self::HEADER_CONTENT_TYPE,
                                self::CONTENT_TYPE_MULTIPAR_MIXED . '; boundary=' . $this->getBoundary());
        $this->getHeader()->add(self::HEADER_X_MAILER, 'PHP v' . phpversion());

        if (false === $this->getHeader()->has(self::HEADER_REPLY_TO)) {
            $this->getHeader()->add(self::HEADER_REPLY_TO, $this->getFrom());
        }
        if (false === $this->getHeader()->has(self::HEADER_RETURN_PATH)) {
            $this->getHeader()->add(self::HEADER_RETURN_PATH, $this->getFrom());
        }

        return (string)$this->getHeader();
    }

    /**
     * Returns header class instance.
     *
     * @return Mail_Header|null
     */
    protected function getHeader() {
        if (null === $this->header) {
            $this->header = new Mail_Header();
        }

        return $this->header;
    }

    /**
     * Returns a Mail_Address class.
     *
     * @param string|Mail_Address $email
     * @param null|string         $name
     * @param string              $charset
     *
     * @return Mail_Address
     */
    protected static function getMailAddress($email, $name = null, $charset = self::CHARSET_UTF_8) {
        if (!($email instanceof Mail_Address)) {
            $name = $name ? self::encode($name, $charset) : $name;
            $email = new Mail_Address($email, $name);
        }

        return $email;
    }
}