<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_send_mail_with_smtp
 * @version   1.0.0
 * @since     2013.10.29.
 */
/**
 * Class Mail_Adapter_Abstract
 */
abstract class Mail_Adapter_Abstract {
    protected $email = null;

    /**
     * Log messages.
     *
     * @var array
     */
    protected $logs = array();

    /**
     * Constructor
     */
    public function __construct($options = null) {
        if (null !== $options) {
            $this->setOptions((array)$options);
        }
    }

    /**
     * Add a log message.
     *
     * @param string $msg
     *
     * @return $this
     */
    public function addLog($msg) {
        array_push($this->logs, $msg);

        return $this;
    }

    /**
     * Returns log messages as array.
     *
     * @return array
     */
    public function getLog() {
        return $this->logs;
    }

    /**
     * Sets adapter options.
     *
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options) {
        foreach ($options as $name => $value) {
            $methodName = 'set' . ucfirst($name);
            if (method_exists($this, $methodName)) {
                call_user_func_array(array(
                                         $this,
                                         $methodName
                                     ),
                                     array($value));
            }
        }

        return $this;
    }

    /**
     * Send the mail.
     *
     * @param Mail $email
     *
     * @return mixed
     */
    public final function send(Mail $email) {
        $this->email = $email;

        return $this->_send();
    }

    /**
     * Returns Mail instance.
     *
     * @return Mail
     */
    protected function getEmail() {
        return $this->email;
    }

    /**
     * Returns the sender of the mail.
     *
     * @return Mail_Address
     */
    protected function getFrom() {
        return $this->getEmail()->getFrom();
    }

    /**
     * Returns the primary recipient of the message.
     *
     * @return array
     */
    protected function getTo() {
        return $this->getEmail()->getTo();
    }

    /**
     * Returns the subject of the mail.
     *
     * @return string
     */
    protected function getSubject() {
        return (string)$this->getEmail()->getSubject();
    }

    /**
     * Returns this mail body as string.
     *
     * @return string
     */
    protected function getBody() {
        return $this->getEmail()->bodyToString();
    }

    /**
     * Returns this mail headers as string.
     *
     * @return string
     */
    protected function getHeaders() {
        return $this->getEmail()->headerToString();
    }

    /**
     * Send the mail.
     *
     * @return mixed
     */
    protected abstract function _send();
} 