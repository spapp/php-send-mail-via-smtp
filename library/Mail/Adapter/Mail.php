<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_send_mail_with_smtp
 * @version   1.0.0
 * @since     2013.10.30.
 */
require_once('Abstract.php');
/**
 * Class Mail_Adapter_Mail
 */
class Mail_Adapter_Mail extends Mail_Adapter_Abstract {
    /**
     * Returns the primary recipient of the message.
     *
     * @return string
     */
    protected function getTo() {
        $to = parent::getTo();
        $tos = array();

        foreach ($to as $value) {
            array_push($tos, (string)$value);
        }

        return implode(', ', $tos);
    }

    /**
     * Send the mail with mail function.
     *
     * @return bool|mixed
     */
    protected function _send() {
        return mail($this->getTo(),
                    (string)$this->getSubject(),
                    (string)$this->getBody(),
                    (string)$this->getHeaders());
    }
} 