<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_send_mail_with_smtp
 * @version   1.0.0
 * @since     2013.10.30.
 */

require_once('Abstract.php');
/**
 * Class Mail_Adapter_Smtp
 *
 * @link http://www.rfc-base.org/txt/rfc-2821.txt RFC 2821
 */
class Mail_Adapter_Smtp extends Mail_Adapter_Abstract {
    /**
     * Default SMTP host
     *
     * @var string
     */
    const DEFAULT_HOST = 'localhost';

    /**
     * Default SMTP port.
     *
     * @var int
     */
    const DEFAULT_PORT = 25;

    /**
     * Default socket connection timeout.
     *
     * @var int
     */
    const DEFAULT_SOCKET_TIMEOUT = 30;

    /**
     * Default SMTP command timeout
     *
     * @see RFC 2821 4.5.3.2 Timeouts
     * @var int
     */
    const TIMEOUT_DEFAULT = 300;

    /**
     * Initial 220 message timeout.
     *
     * @var int
     */
    const TIMEOUT_CONNECTION = self::TIMEOUT_DEFAULT;

    /**
     * SMTP HELO and EHLO command timeout
     *
     * @see RFC 2821 4.5.3.2 Timeouts
     * @var int
     */
    const TIMEOUT_HELO = self::TIMEOUT_DEFAULT;

    /**
     * SMTP MAIL command timeout
     *
     * @see RFC 2821 4.5.3.2 Timeouts
     * @var int
     */
    const TIMEOUT_MAIL = self::TIMEOUT_DEFAULT;

    /**
     * SMTP RCPT command timeout
     *
     * @see RFC 2821 4.5.3.2 Timeouts
     * @var int
     */
    const TIMEOUT_RCPT = self::TIMEOUT_DEFAULT;

    /**
     * SMTP DATA initiation command timeout
     *
     * @see RFC 2821 4.5.3.2 Timeouts
     * @var int
     */
    const TIMEOUT_DATA_INITIATION = 120;

    /**
     * SMTP DATA termination command timeout
     *
     * @see RFC 2821 4.5.3.2 Timeouts
     * @var int
     */
    const TIMEOUT_DATA_TERMINATION = 600;

    /**
     * SMTP QUIT command timeout
     *
     * @see RFC 2821 4.5.3.2 Timeouts
     * @var int
     */
    const TIMEOUT_QUIT = self::TIMEOUT_DEFAULT;

    /**
     * Current SMTP server host.
     *
     * @var string
     */
    protected $host = null;

    /**
     * Current SMTP server port.
     *
     * @var string
     */
    protected $port = null;

    /**
     * Username.
     *
     * @var string
     */
    protected $username = null;

    /**
     * Password.
     *
     * @var string
     */
    protected $password = null;

    /**
     * Current socket resource id.
     *
     * @var null
     */
    protected $socket = null;

    /**
     * Current transport type.
     *
     * @var string
     */
    protected $transport = 'tcp';

    /**
     * Class destructor to cleanup open socket.
     *
     * @return void
     */
    public function __destruct() {
        $this->close();
    }

    /**
     * Set stream timeout.
     *
     * @param integer $timeout
     *
     * @return boolean
     */
    protected function setTimeout($timeout) {
        return stream_set_timeout($this->socket, $timeout);
    }

    /**
     * Returns socket connection timeout.
     *
     * @return int
     */
    protected function getConnectionTimeout() {
        $timeout = intval(ini_get('default_socket_timeout'));

        if (!$timeout) {
            $timeout = self::DEFAULT_SOCKET_TIMEOUT;
        }

        return $timeout;
    }

    /**
     * Sets SMTP server host.
     *
     * @param string $host
     *
     * @return $this
     */
    protected function setHost($host) {
        $this->host = $host;

        return $this;
    }

    /**
     * Returns SMTP server host.
     *
     * @return string
     */
    protected function getHost() {
        if (null === $this->host) {
            $this->host = self::DEFAULT_HOST;
        }

        return $this->host;
    }

    /**
     * Sets SMTP server port.
     *
     * @param int $port
     *
     * @return $this
     */
    protected function setPort($port) {
        $this->port = $port;

        return $this;
    }

    /**
     * Returns SMTP server port.
     *
     * @return int
     */
    protected function getPort() {
        if (null === $this->port) {
            $this->port = intval(ini_get('smtp_port'));
        }
        if (!$this->port) {
            $this->port = self::DEFAULT_PORT;
        }

        return $this->port;
    }

    /**
     * Sets username.
     *
     * @param string $username
     *
     * @return $this
     */
    protected function setUsername($username) {
        $this->username = $username;

        return $this;
    }

    /**
     * Returns username.
     *
     * @return string
     */
    protected function getUsername() {
        return $this->username;
    }

    /**
     * Sets pasword.
     *
     * @param string $password
     *
     * @return $this
     */
    protected function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Returns password.
     *
     * @return string
     */
    protected function getPassword() {
        return $this->password;
    }

    /**
     * Returns transport type.
     *
     * @return string
     */
    protected function getTransport() {
        return $this->transport;
    }

    /**
     * Returns remote server address.
     *
     * @return string
     */
    protected function getRemote() {
        return $this->getTransport() . '://' . $this->getHost() . ':' . $this->getPort();
    }

    /**
     * Returns the primary recipient of the message.
     *
     * @return string
     */
    protected function getTo() {
        $to = parent::getTo();
        $returns = array();

        foreach ($to as $value) {
            array_push($returns, $value->toString());
        }

        return implode(', ', $returns);
    }

    /**
     * Connect to remote SMTP server.
     *
     * @return bool
     */
    protected function connect() {
        $errorString = 'Unknown error.';
        $errorNo = 0;

        if (!is_resource($this->socket)) {
            $this->socket = @stream_socket_client($this->getRemote(),
                                                  $errorNo,
                                                  $errorString,
                                                  $this->getConnectionTimeout());
        }

        if (!is_resource($this->socket)) {
            Mail::throwException($errorString, $errorNo);
        }

        return is_resource($this->socket);
    }

    /**
     * Disconnect from remote host and close socket.
     *
     * @return void
     */
    protected function close() {
        if (is_resource($this->socket)) {
            fclose($this->socket);
        }
    }

    /**
     * Sends a command to the SMTP server.
     *
     * @param string $data
     *
     * @return void
     */
    protected function sendCommand($data) {
        if (!is_resource($this->socket)) {
            Mail::throwException('No connection ' . $this->getHost());
        }

        fputs($this->socket, $data . Mail::EOL);

        $this->addLog($data);
    }

    /**
     * Returns reply of the last command.
     *
     * @param int $timeout
     *
     * @return string
     */
    protected function getResponse($timeout = self::TIMEOUT_DEFAULT) {
        $this->setTimeout($timeout);

        $response = trim(fgets($this->socket, 1024));

        $this->addLog('    ' . $response);

        $metaData = stream_get_meta_data($this->socket);

        if (true === $metaData['timed_out']) {
            $response = '0 The SMTP server has timed out.';
        }

        return $response;
    }

    /**
     * Checks the response code.
     *
     * Normally, the response to EHLO will be a multiline reply.
     * Each line contains a keyword and, optionally, one or more parameters.
     * Following the normal syntax for multiline replies,
     * these keyworks follow the code (250) and a hyphen for all but the last
     * line, and the code and a space for the last line.
     *
     * @see RFC 2821 4.1.1.1 Syntax part
     *
     * @param int|array $code
     * @param int       $timeout
     *
     * @return mixed
     */
    protected function checkResponse($code, $timeout) {
        $errorMessage = array();

        if (!is_array($code)) {
            $code = array($code);
        }

        do {
            $response = trim($this->getResponse($timeout));
            list($responseCode, $message) = preg_split('~[ -]~', $response, 2, PREG_SPLIT_DELIM_CAPTURE);
            $noMore = (bool)preg_match('~^[0-9]{3} ~', $response);

            if (!in_array((int)$responseCode, $code)) {
                array_push($errorMessage, $message);
            }
        } while (false === $noMore);

        $responseCode = intval($responseCode);

        if (count($errorMessage) > 0) {
            Mail::throwException(implode(' ', $errorMessage));
        }

        return $responseCode;
    }

    /**
     * These commands are used to identify the SMTP client to the SMTP server.
     *
     * @see RFC 2821 4.1.1.1
     *
     * @return void
     * @throws Exception
     */
    protected function sendHelo() {
        try {
            $this->sendCommand('EHLO ' . Mail::getHostName());
            $this->checkResponse(250, self::TIMEOUT_HELO);
        } catch (Exception $error) {
            try {
                $this->sendCommand('HELO ' . Mail::getHostName());
                $this->checkResponse(250, self::TIMEOUT_HELO);
            } catch (Exception $error) {
                throw $error;
            }
        }
    }

    /**
     * This command is used to initiate a mail transaction.
     *
     * @see RFC 2821 4.1.1.2
     *
     * @return void
     * @throws Exception
     */
    protected function sendMail() {
        $this->sendCommand('MAIL FROM: <' . $this->getFrom()->toString() . '>');
        $this->checkResponse(250, self::TIMEOUT_MAIL);
    }

    /**
     * This command is used to identify an individual recipient of the mail data.
     *
     * @see RFC 2821 4.1.1.3
     *
     * @return void
     * @throws Exception
     */
    protected function sendRcpt() {
        $this->sendCommand('RCPT TO: <' . $this->getTo() . '>');
        $this->checkResponse(array(
                                 250,
                                 251
                             ),
                             self::TIMEOUT_MAIL);
    }

    /**
     * This command causes the mail data to be appended to the mail data buffer.
     *
     * @see RFC 2821 4.1.1.4
     *
     * @return void
     * @throws Exception
     */
    protected function sendData() {
        $this->sendCommand('DATA');
        $this->checkResponse(354, self::TIMEOUT_DATA_INITIATION);

        $headers = explode(Mail::EOL, $this->getHeaders());

        foreach ($headers as $header) {
            $this->sendCommand($header);
        }
        // TODO chanked
        $this->sendCommand($this->getBody());
        // @see RFC 2821 4.5.2
        $this->sendCommand('.');
        $this->checkResponse(250, self::TIMEOUT_DATA_TERMINATION);
    }

    /**
     * This command specifies that the receiver MUST send an OK reply,
     * and then close the transmission channel.
     *
     * @see RFC 2821 4.1.1.10
     *
     * @return void
     * @throws Exception
     */
    protected function sendQuit() {
        $this->sendCommand('QUIT');
        $this->checkResponse(221, self::TIMEOUT_QUIT);
    }

    /**
     * User authentication.
     *
     * @return void
     * @throws Exception
     */
    protected function sendAuth() {
        // TODO other authentication type: plain, login, cram-md5
        $this->sendCommand('AUTH LOGIN');
        $this->checkResponse(334, self::TIMEOUT_DEFAULT);
        $this->sendCommand(base64_encode($this->getUsername()));
        $this->checkResponse(334, self::TIMEOUT_DEFAULT);
        $this->sendCommand(base64_encode($this->getPassword()));
        $this->checkResponse(235, self::TIMEOUT_DEFAULT);
    }

    /**
     * Send the mail via smtp.
     *
     * @return bool|mixed
     */
    protected function _send() {
        $isConnected = $this->connect();
        $this->checkResponse(220, self::TIMEOUT_CONNECTION);

        if (true === $isConnected) {
            $this->sendHelo();
            // TODO secure
            if (null !== $this->getUsername() and null !== $this->getPassword()) {
                $this->sendAuth();
            }
            $this->sendMail();
            $this->sendRcpt();
            $this->sendData();
            $this->sendQuit();
        }

        return $isConnected;
    }
} 