<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_send_mail_with_smtp
 * @version   1.0.0
 * @since     2013.10.18.
 */
/**
 * Class Mail_Exception
 */
class Mail_Exception extends Exception {

}