<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_send_mail_with_smtp
 * @version   1.0.0
 * @since     2013.10.21.
 */
/**
 * Class Header
 */
class Mail_Header {
    /**
     * Contains headers.
     *
     * @var array
     */
    protected $headers = array();

    /**
     * Constructor
     */
    public function __construct() {
        $this->clear();
    }

    /**
     * Returns adjusted header.
     *
     * @param string $name header name
     *
     * @return null|array
     */
    public function get($name) {
        $returns = null;
        if (true === $this->has($name)) {
            $returns = $this->headers[$name];
        }

        return $returns;
    }

    /**
     * Returns TRUE if the header is exists.
     *
     * @param string $name
     *
     * @return bool
     */
    public function has($name) {
        return array_key_exists($name, $this->headers);
    }

    /**
     * Adjusts a header.
     *
     * If the $append is TRUE then append value otherwise set it.
     *
     * @param string $name  header name
     * @param string $value header value
     * @param bool   $append
     *
     * @return $this
     */
    public function add($name, $value, $append = false) {
        if (false === $append or false === $this->has($name)) {
            $this->headers[$name] = array();
        }

        array_push($this->headers[$name], $value);

        return $this;
    }

    /**
     * Clear all headers.
     *
     * @return $this
     */
    public function clear() {
        $this->headers = array();

        return $this;
    }

    /**
     * Magice function.
     *
     * @see Mail_Header::toString
     * @see http://php.net/manual/en/language.oop5.magic.php PHP Magic Methods
     *
     * @return string
     */
    public function __toString() {
        return $this->toString();
    }

    /**
     * Returns headers as a string.
     *
     * @return string
     */
    public function toString() {
        $headers = array();

        foreach ($this->headers as $name => $value) {
            $values = array();

            foreach ($value as $v) {
                array_push($values, (string)$v);
            }

            array_push($headers, $name . ': ' . implode(', ', $values));
        }

        return implode(Mail::EOL, $headers);
    }
}