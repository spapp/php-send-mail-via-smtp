<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_send_mail_with_smtp
 * @version   1.0.0
 * @since     2013.10.29.
 */
/**
 * Class Adapter
 */
class Mail_Adapter {
    const ADAPTER_PREFIX = 'Mail_Adapter_';

    /**
     * Constructor
     */
    protected function __construct() {

    }

    /**
     * Returns a mail sender adapter.
     *
     * @param string|array $adapterName
     * @param null|array   $options
     *
     * @return Mail_Adapter_Abstract
     */
    public static function factory($adapterName, $options = null) {
        if (is_array($adapterName)) {
            $options = $adapterName['params'];
            $adapterName = $adapterName['adapter'];
        } elseif (!is_string(trim($adapterName))) {
            Mail::throwException('The adapterName must be a string or an array.');
        }

        $className = self::ADAPTER_PREFIX . ucfirst(trim($adapterName));

        Mail::loadClass($className);

        return new $className($options);
    }
} 