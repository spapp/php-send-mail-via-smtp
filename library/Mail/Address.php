<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_send_mail_with_smtp
 * @version   1.0.0
 * @since     2013.10.18.
 */
/**
 * Class Mail_Address
 */
class Mail_Address {
    /**
     * At symbol.
     *
     * @var string
     */
    const CHAR_AT = '@';

    /**
     * Fullstop symbol.
     *
     * @var string
     */
    const CHAR_FULLSTOP = '.';

    /**
     * Greater-than symbol.
     *
     * @var string
     */
    const CHAR_GT = '<';

    /**
     * Less-than symbol.
     *
     * @var string
     */
    const CHAR_LT = '>';

    /**
     * The email address of the owner's name.
     *
     * @var string
     */
    protected $name = '';

    /**
     * The email address local part.
     *
     * @var string
     */
    protected $mailbox = '';

    /**
     * The email address domain part.
     *
     * @var string
     */
    protected $domain = '';

    /**
     * Constructor
     */
    public function __construct($address, $name = '') {
        $this->setName($name);
        $this->prepareAddress($address);
    }

    /**
     * Sets the email address of the owner's name.
     *
     * @param $name
     *
     * @return $this
     */
    public function setName($name) {
        $this->name = is_string($name) ? trim($name) : '';

        return $this;
    }

    /**
     * Returns the email address of the owner's name.
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Sets the email address local part.
     *
     * @param string $mailbox
     *
     * @return $this
     */
    public function setMailbox($mailbox) {
        $this->mailbox = $mailbox;

        return $this;
    }

    /**
     * Returns the email address local part.
     *
     * @return string
     */
    public function getMailbox() {
        return $this->mailbox;
    }

    /**
     * Sets the email address domain part.
     *
     * @param string $domain
     *
     * @return $this
     */
    public function setDomain($domain) {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Returns the email address domain part.
     *
     * @return string
     */
    public function getDomain() {
        return $this->domain;
    }

    /**
     * Returns the email address (mailbox and domain).
     *
     * @return string
     */
    public function toString() {
        return $this->mailbox . self::CHAR_AT . $this->domain;
    }

    /**
     * Magice function.
     *
     * Returns email address with name.
     *      eg: John Doe <john.doe@example.tld>
     *
     * @return string
     */
    public function __toString() {
        return $this->name . ' ' . self::CHAR_GT . $this->toString() . self::CHAR_LT;
    }

    /**
     * Returns TRUE if it is a valid email address.
     *
     * @todo
     *
     * @return bool
     */
    public function isValid() {
        // TODO
        return true;
    }

    /**
     * Prepare email address.
     *
     * @param string $address
     *
     * @return $this
     */
    protected function prepareAddress($address) {
        $address = explode(self::CHAR_AT, $address);

        if (isset($address[0])) {
            $this->setMailbox($address[0]);
        }
        if (isset($address[1])) {
            $this->setDomain($address[1]);
        }

        return $this;
    }
}