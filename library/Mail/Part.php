<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_send_mail_with_smtp
 * @version   1.0.0
 * @since     2013.10.25.
 */
/**
 * Class Mail_Part
 */
class Mail_Part {
    /**
     * Part character set.
     *
     * Default: Mail::CHARSET_UTF_8
     *
     * @var string
     */
    protected $charset = Mail::CHARSET_UTF_8;

    /**
     * Part content type.
     *
     * Default: Mail::CONTEN_TYPE_TEXT
     *
     * @var string
     */
    protected $type = Mail::CONTENT_TYPE_TEXT;

    /**
     * Part content.
     *
     * @var string
     */
    protected $content = '';

    /**
     * This field contains the part headers.
     *
     * @var Mail_Header
     */
    protected $header = null;

    /**
     * Constructor
     */
    public function __construct($content = null) {
        if (is_string($content)) {
            $this->setContent($content);
        }
    }

    /**
     * Magice function.
     *
     * Returns this part as string.
     *
     * @see http://php.net/manual/en/language.oop5.magic.php PHP Magic Methods
     *
     * @return string
     */
    public function __toString() {
        $text = array(
            $this->headerToString(),
            '',
            $this->getContent()
        );

        return implode(Mail::EOL, $text);
    }

    /**
     * Returns this mail headers as string.
     *
     * @return string
     */
    protected function headerToString() {
        if (false === $this->getHeader()->has(Mail::HEADER_CONTENT_TYPE)) {
            $this->header(Mail::HEADER_CONTENT_TYPE, $this->getType() . '; charset="' . $this->getCharset() . '"');
        }

        return (string)$this->getHeader();
    }

    /**
     * Returns header class instance.
     *
     * @return Mail_Header|null
     */
    protected function getHeader() {
        if (null === $this->header) {
            $this->header = new Mail_Header();
        }

        return $this->header;
    }

    /**
     * Add a header.
     *
     * @param string $name
     * @param string $value
     * @param bool   $append
     */
    public function header($name, $value, $append = false) {
        $this->getHeader()->add($name, $value, $append);
    }

    /**
     * Returns the part content type.
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Sets the part content type
     *
     * @param string $typet
     *
     * @return $this
     */
    public function setType($typet) {
        $this->type = $typet;

        return $this;
    }

    /**
     * Returns the part character set.
     *
     * @return string
     */
    public function getCharset() {
        return $this->charset;
    }

    /**
     * Sets the part character set.
     *
     * @param string $charset
     *
     * @return $this
     */
    public function setCharset($charset) {
        $this->charset = $charset;

        return $this;
    }

    /**
     * Returns the part content value.
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Sets the part content value.
     *
     * @param string $content
     *
     * @return $this
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }
}