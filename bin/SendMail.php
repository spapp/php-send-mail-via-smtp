<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_send_mail_with_smtp
 * @version   1.0.0
 * @since     2013.11.05.
 */
require_once(dirname(__FILE__) . '/../library/Mail.php');

Mail::loadClass('Mail_Adapter_Smtp');
/**
 * Helper class to send_mail.php script.
 *
 * Class SendMail
 */
class SendMail extends Mail {
    /**
     * @var SendMail
     */
    protected static $instance = null;

    /**
     * Argument aliases.
     *
     * @var array
     */
    protected static $aliases = array(
        'h' => 'help',
        'f' => 'from',
        's' => 'subject',
        'c' => 'charset',
        'a' => 'adapter',
        'H' => 'host',
        'P' => 'port',
        'u' => 'username',
        'p' => 'password',
        't' => 'to',
        'v' => 'verbose'
    );

    /**
     * Allowed arguments.
     *
     * @var array
     */
    protected static $allowed = array(
        'help',
        'from',
        'subject',
        'charset',
        'adapter',
        'host',
        'port',
        'username',
        'password',
        'to',
        'cc',
        'bcc',
        'text',
        'html',
        'verbose'
    );

    /**
     * Email options. Contains the default options.
     *
     * @var array
     */
    protected $options = array(
        'charset' => Mail::CHARSET_UTF_8,
        'adapter' => array(
            'adapter' => 'smtp',
            'params'  => array(
                'host' => Mail_Adapter_Smtp::DEFAULT_HOST,
                'port' => Mail_Adapter_Smtp::DEFAULT_PORT
            )
        )
    );

    /**
     * Required mail fields.
     *
     * @var array
     */
    protected $errors = array(
        'from'    => 'The sender of the mail is required.',
        'to'      => 'The primary recipient of the mail is required.',
        'subject' => 'The subject of the mail is required.',
        'body'    => 'The message of the mail is required.'
    );

    /**
     * Help argument flag.
     *
     * @var bool
     */
    protected $help = false;

    /**
     * Verbose argument flag.
     *
     * @var bool
     */
    protected $verbose = false;

    /**
     * Help argument is sended.
     *
     * @return bool
     */
    public function isHelp() {
        return $this->help;
    }

    /**
     * Verbose argument is sended.
     *
     * @return bool
     */
    public function isVerbose() {
        return $this->verbose;
    }

    /**
     * Returns the long option name if it is valid otherwise return NULL.
     *
     * @param string $argument
     *
     * @return string|null
     */
    protected function getValidArgument($argument) {
        $argument = preg_replace('~^[-]+~', '', $argument);

        if (array_key_exists($argument, self::$aliases)) {
            $argument = self::$aliases[$argument];
        }
        if (!in_array($argument, self::$allowed)) {
            $argument = null;
        }

        return $argument;
    }

    /**
     * Remove a error message.
     *
     * If the required field is sended then it is removed the error list.
     *
     * @param string $key
     */
    protected function removeError($key) {
        if (array_key_exists($key, $this->errors)) {
            unset($this->errors[$key]);
        }
    }

    /**
     * Returns current error messages.
     *
     * @return string
     */
    public function getErrors() {
        return implode(PHP_EOL, $this->errors);
    }

    /**
     * Parse arguments.
     *
     * @param array $arguments
     */
    public function parseArguments(array $arguments) {

        while ($arg = array_shift($arguments)) {
            $arg = $this->getValidArgument($arg);

            switch (true) {
                case ('verbose' === $arg):
                    $this->verbose = true;
                case !$arg:
                    continue;
                case ('help' === $arg):
                    $this->help = true;

                    return;
            }

            $this->removeError($arg);

            switch (true) {
                case method_exists($this, 'set' . ucfirst($arg)):
                    call_user_func_array(array(
                                             $this,
                                             'set' . ucfirst($arg)
                                         ),
                                         (array)array_shift($arguments));
                    break;
                case method_exists($this, 'add' . ucfirst($arg)):
                    call_user_func_array(array(
                                             $this,
                                             'add' . ucfirst($arg)
                                         ),
                                         (array)array_shift($arguments));
                    break;
                case ('text' === $arg):
                    $content = file_get_contents(array_shift($arguments));
                    $this->addTextBody($content);
                    $this->removeError('body');
                    break;
                case ('html' === $arg):
                    $content = file_get_contents(array_shift($arguments));
                    $this->addHtmlBody($content);
                    $this->removeError('body');
                    break;
                case ('adapter' === $arg):
                    $this->options['adapter']['adapter'] = array_shift($arguments);
                    break;
                default:
                    $this->options['adapter']['params'][$arg] = array_shift($arguments);
            }
        }

        if (false === $this->isHelp()) {
            $this->setAdapter($this->options['adapter']['adapter'], $this->options['adapter']['params']);
        }
    }

    /**
     * Returns TRUE if the all required opsion is sended.
     *
     * @return bool
     */
    public function isValid() {
        return (bool)(count($this->errors) < 1);
    }

    /**
     * Returns class instance.
     *
     * @return SendMail
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self(array());
        }

        return self::$instance;
    }
}