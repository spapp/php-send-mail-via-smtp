<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   php_send_mail_with_smtp
 * @version   1.0.0
 * @since     2013.11.04.
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('APPLICATION_PATH', dirname(dirname(__FILE__)));

require_once(APPLICATION_PATH . '/bin/SendMail.php');

if ('cli' !== substr(php_sapi_name(), 0, 3)) {
    SendMail::throwException('Use this script in CLI.');
}

// Remove zero argument.
array_shift($argv);

if (count($argv) < 1) {
    $argv[] = '-h';
}

SendMail::getInstance()->parseArguments($argv);

if (true === SendMail::getInstance()->isHelp()) {
    echo file_get_contents(APPLICATION_PATH . '/doc/man.txt');
} elseif (true === SendMail::getInstance()->isValid()) {
    if (SendMail::getInstance()->isVerbose()) {
        echo 'Email parameters is valid.' . PHP_EOL;
    }
    try {
        if (SendMail::getInstance()->isVerbose()) {
            echo 'Email sending.' . PHP_EOL;
        }
        SendMail::getInstance()->send();
    } catch (Exception $e) {
        echo PHP_EOL . SendMail::getInstance()->getLogAsString() . PHP_EOL;
        SendMail::throwException($e->getMessage(), $e->getCode(), $e);
    }
    if (SendMail::getInstance()->isVerbose()) {
        echo 'Email sending is complete.' . PHP_EOL;
        echo 'SMTP log:' . PHP_EOL . PHP_EOL;
        echo SendMail::getInstance()->getLogAsString() . PHP_EOL;
    }
} else {
    SendMail::throwException(SendMail::getInstance()->getErrors());
}
